class PagesController < ApplicationController
  before_action :set_theme


  def start_resources
  end


  def start_activities
  end


  def journeys_resources
  end


  def journeys_activities
  end


  def activities_activities
  end


  def activities_resources
  end


  def routes_activities
  end


  def routes_resources
  end


  private


  def set_theme
    @mode, @theme =
      if request.original_url.split('/').last == 'activities'
        ['activity', 'theme-blue']
      else
        ['resource', 'theme-red']
      end

  end

end
