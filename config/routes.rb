Rails.application.routes.draw do
  get '/start/resources', to: 'pages#start_resources'
  get '/activities/resources', to: 'pages#activities_resources'
  get '/journeys/resources', to: 'pages#journeys_resources'
  get '/routes/resources', to: 'pages#routes_resources'

  get '/start/activities', to: 'pages#start_activities'
  get '/activities/activities', to: 'pages#activities_activities'
  get '/journeys/activities', to: 'pages#journeys_activities'
  get '/routes/activities', to: 'pages#routes_activities'

  root to: 'pages#start_resources'
end
